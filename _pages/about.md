---
layout: about
permalink: /
title: <strong>Sandeep</strong> Suman
description: University Department of Mathematics, TMBU, Bhagalpur.

profile:
  align: right
  image: prof_pic.jpg
  # address: >
  #   <p>Department of Mathematics</p>
  #   <p>Old PG Campus</p>
  #   <p>TMBU, Bhagalpur</p>

news: true
social: true
---


#### Research Interest

- Nonlinear Optimization

#### Hobby

- History of Mathematics
- Programming
- Chess

To know about the courses taught by me, please visit the [teaching page](/teaching/).