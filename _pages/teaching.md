---
layout: page
permalink: /teaching/
title: teaching
description: Information about the course I am teaching. For more detail click on course links.
---

#### Current Courses 

- [Linear Algebra](https://sandeepsuman.com/linear-algebra/) in Semester I
- [Complex Analysis](https://sandeepsuman.com/complex-analysis/) in Semester II
- Analytical Mechanics in Semester III

***

#### Past Courses

- [Abstract Algebra](https://sandeepsuman.com/abstract-algebra/) in semester I
- PDE in semester IV